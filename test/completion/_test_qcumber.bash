# bash completion for test fun
_test_qcumber() {
  COMPREPLY=()
  local word="${COMP_WORDS[COMP_CWORD]}"
  local prev_word="${COMP_WORDS[COMP_CWORD-1]}"
  local word_before_previous="${COMP_WORDS[COMP_CWORD-2]}"
  local line=${COMP_LINE}
  if [ "$COMP_CWORD" -eq 1 ]; then
	top_opts="--help gen_test_yaml --run_file --gold_standard_file \
    --trigger_overwrite update_default_yaml util"
    if [[ ${word} == -* ]] ; then
        top_opts="-h --help -i --runfile -g --gold_standard_file\
        --trigger_overwrite"
    fi
    COMPREPLY=( $(compgen -W "${top_opts}" -- "$word") )
  else
    local completions=""
    prelim_reply=()
    case "$line" in
        *update_default_yaml*)
            if [[ "$COMP_CWORD" -eq 2 ]] ; then
                completions="-h --help"
            fi
            prelim_reply=( $(compgen -W "${completions}" -- "$word") );;
        *gen_test_yaml*)
            if [[ "$COMP_CWORD" -eq 2 ]] ; then
                completions="-h --help"
            fi
            prelim_reply=( $(compgen -W "${completions}" -- "$word") );;
        *util*)
            if [[ "${prev_word}" = "util" ]] ; then
                if [[ "$COMP_CWORD" -eq 2 ]] ; then
                    completions="--help --run_info --test_regex"
                    if [[ ${word} == -* ]] ; then
                        completions="-h --help -r --test_regex -i --run_info"
                    fi
                prelim_reply=( $(compgen -W "${completions}" -- "$word" ) )
                fi
            else
                if [[ "${prev_word}" = "-r" ]] || [[ "$prev_word" = "--test_regex" ]] ; then
                    completions="\'"'s/(pattern)//g'"\'"
                    prelim_reply=( $(compgen -W "${completions}" -- ) )
                elif [[ "${prev_word}" = "-i" ]] || [[ "${prev_word}" = "--run_info" ]] ; then
                    prelim_reply=( $(compgen -d -W "${completions}" -- "$word") )
                fi
            fi;;
        *)
            if [[ "${word}" == -* ]] ; then
                completions="-h --help -i --runfile -g --gold_standard_file\
                --trigger_overwrite"
                prelim_reply=( $(compgen -W "${completions}" -- "$word") )
            fi;;
        esac



    #local words=("${COMP_WORDS[@]}")
    #unset words[0]
    #unset words[$COMP_CWORD]
    if [ ${#prelim_reply[@]} -eq 0 ] ; then
        prelim_reply=( $(compgen -f -W "${completions}" -- "$word"))
    fi
    COMPREPLY=( ${prelim_reply[@]} )
  fi
}

complete -F _test_qcumber ./test_qcumber2.py
