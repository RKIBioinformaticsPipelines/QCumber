import sys
#--------------------------------------------< RULES >-----------------------------------------------------------------#

# Run FastQC on raw data
rule fastqc_raw:
    input:
        lambda wildcards: sample_dict[wildcards.sample]
    output:
        qc_summary=temp(fastqc_path + "/raw/{sample}_fastqc/fastqc_data.txt"),
        zip= fastqc_path + "/raw/{sample}_fastqc.zip",
        html = temp(fastqc_path + "/raw/{sample}_fastqc.html"),
        folder = temp(fastqc_path + "/raw/{sample}_fastqc"),
    message:
        "Run FastQC on raw data."
    threads:
        max_threads
    log:
        log_path + "/logfile.fastqc.txt"
    run:
        shell("fastqc {input} -o {path}/raw/ "
              "--extract -t {threads} >>  {log} 2>&1 ", path = fastqc_path )
        if ("{path}/raw/{name}_fastqc".format(name=get_name(str(input)),
                                             path=fastqc_path)
            != str(output.folder)):
                shell("rsync -r --remove-source-files"
                      " {path}/raw/{name}_fastqc/* {output.folder}",
                      name=get_name(str(input)), path=fastqc_path )
                #shell("mv {/fastqc_data.txt "
                #      "{output.qc_summary}",
                #      name=get_name(str(input)), path=fastqc_path)
                shell("mv -f {path}/raw/{name}_fastqc.html {output.html}",
                      name=get_name(str(input)), path=fastqc_path)
                shell("mv -f {path}/raw/{name}_fastqc.zip {output.zip}",
                      name=get_name(str(input)), path=fastqc_path)


rule fastqc_trimmed:
    input:
        trimming_path + "/{sample}.fastq.gz"
    output:
        fastqc_path + "/trimmed/{sample}_fastqc/fastqc_data.txt",
        fastqc_path + "/trimmed/{sample}_fastqc.zip",
        temp(fastqc_path + "/trimmed/{sample}_fastqc.html"),
        folder = temp(fastqc_path + "/trimmed/{sample}_fastqc"),
    threads:
        max_threads
    log:
        log_path + "/logfile.trimmed.fastqc.txt"
        #temp(log_path + "/{sample}.fastqc.trimmed.log")
    message:
        "Run FastQC for trimmed data."
    run:
        # print('sizes:', ' '.join(['%s:%i:' % (x,os.path.getsize(x))
        #                           for x in input]), file=sys.stderr)
        shell(
            "if [ `zcat '{input}' | head -n 1 | wc -c ` -eq 0 ];"
            "then touch {output};"
            "else fastqc {input} "  # "-Djava.awt.headless=true "
            "-o $(dirname {output.folder})"
            " --extract -t {threads} >>  {log} 2>&1;  fi ")


rule trimmomatic_stats_2_csv:
    input:
        fastqc_path + "/raw/{sample}_{read}_fastqc_data.txt"
    output:
        temp(fastqc_path + "/raw/{sample}_{read}_fastqc_stat.csv")
    run:
        pass

